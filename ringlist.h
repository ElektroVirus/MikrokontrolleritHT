#ifndef _RINGLIST_H
#define _RINGLIST_H

#include <stdint.h>

/**
  This module creates a ringlist. It's maximum size is defined by
  LENGTH. The list end position changes when elements are added to
  the list, start position is changed when elements are deleted. 
  List initial size is 0 and it grows with push_back. 
*/

#define RINGLIST_DATATYPE uint8_t

/** This struct is the ringlist. */
typedef struct Ringlist {
    /** Pointer to start of the static list */
    RINGLIST_DATATYPE * arr;
    /** Pointer to the first element in the list */
    RINGLIST_DATATYPE * start;
    /** Pointer to the last element in list. 
	If list is empty, end == 0 */
    RINGLIST_DATATYPE * end;
	/** How many elements are in list */
	unsigned size;
    /** Number of allocated blocs in array */
    unsigned MAX_SIZE;
} ringlist_t;

/**
 * @brief Initializes ringlist.
 * @param list Pointer to Ringlist where list is stored
 * @param length Int, lenght of list. Must be > 1
 */
void init_ringlist(struct Ringlist * list, unsigned length);

/**
 * @brief free_ringlist Frees ringlist
 */
void free_ringlist(struct Ringlist * list);

/**
 * @brief Returns the first element of list without removing it
 * @param list Ringlist 
 * @return First value of list
 */
RINGLIST_DATATYPE peek_front(struct Ringlist * list);

/** 
 * @brief Removes the first element of array.
 * @param list Ringlist
 * @return int First value of list. 
 *	If list is empty, returns 0.
 */
RINGLIST_DATATYPE pop_front(struct Ringlist * list);

/** 
 * @brief adds new element to the back of the list. 
 * @param list Ringlist 
 * @param newval int new value to be added to list
 * @return int
	0: addition successful
	1: addition succesful, list now full
	2: No commits to list, it was and is full.
 */
int push_back(struct Ringlist * list, RINGLIST_DATATYPE newval);

/** 
 * @brief Clears ringlist 
 */
void clear(struct Ringlist * list);

/**
 * @brief This is probably useless since 
 *	struct elements can be accessed without this
 * @param list Ringlist 
 * @return unsigned int Amount of elements in ringlist
 */
unsigned int size(struct Ringlist * list);

/**
 * @brief Prints the array to console. Only for debugging!
 */
void tulosta(struct Ringlist * list);

#endif // RINGLIST_H
