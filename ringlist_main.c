#include <stdio.h>
#include "ringlist.h"

int main()
{
    
    ringlist_t lista;
    
    init_ringlist(&lista, 10);
	
    tulosta(&lista);
    printf("pop:  %d\n", pop_front(&lista));
    tulosta(&lista);
    
    

    int i = 0;
    while (i < 15)
    {
        printf("push %d: %d\n", i+20, push_back(&lista, i+20));
        //printf("push %d: %d\n",i+10, push_back(&lista, i+10));
        tulosta(&lista);
        i++;
    }
    while (i > 10)
    {
        printf("pop:  %d\n", pop_front(&lista));
        tulosta(&lista);
        i--;
    }
    
    return 0;
    
    
    
    tulosta(&lista);
	printf("push 31: %d\n", push_back(&lista, 31));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("push 32: %d\n", push_back(&lista, 32));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("push 33: %d\n", push_back(&lista, 33));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("push 34: %d\n", push_back(&lista, 34));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("push 35: %d\n", push_back(&lista, 35));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("push 36: %d\n", push_back(&lista, 36));
	printf("push 37: %d\n", push_back(&lista, 37));
	printf("push 38: %d\n", push_back(&lista, 38));
	printf("push 39: %d\n", push_back(&lista, 39));
	printf("push 40: %d\n", push_back(&lista, 40));
	printf("push 41: %d\n", push_back(&lista, 41));
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
	printf("pop:  %d\n", pop_front(&lista));
	tulosta(&lista);
	printf("push 37: %d\n", push_back(&lista, 37));
	tulosta(&lista);	
	printf("pop:  %d\n", pop_front(&lista));
	tulosta(&lista);
	printf("pop:  %d\n", pop_front(&lista));
	tulosta(&lista);
	printf("push 39: %d\n", push_back(&lista, 39));
	tulosta(&lista);
	printf("push 39: %d\n", push_back(&lista, 39));
	tulosta(&lista);
	printf("push 40: %d\n", push_back(&lista, 40));
	tulosta(&lista);
	clear(&lista);
    printf("Clear\n");
	tulosta(&lista);
	printf("Size: %d\n", size(&lista));
    
	free_ringlist(&lista);
    
}

