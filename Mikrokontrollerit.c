/*
 * Mikrokontrollerit.c
 *
 * User gives numbers from 0 to 5 with serial port. 
 * Those numbers are stored in variable 'voltage_level_'
 * as an 8-bit value: 0 means 0V up to 255 which means 5V.
 * That is the value which is directly given to PWM output.
 * After an low pass filter the value is measured by ADC
 * and fed back to the PID-controller to correct its value.
 *
 * Pressing the button on board will trigger an interrupt 
 * which will "pause" the operation: voltage drops to 0V
 * on output pin.
 * 
 * Function principle: timer0 controls pwm on pin 6. Timer1 
 * calls regularly ADC for analog pin 5 through an interrupt. 
 * The ADC-ready -interrupt is captured and given to PID-
 * controller which stabilizes the PWM output
 * In main loop there is only a debug print.
 * 
 * Created: 22.3.2018 19:24:45
 *  Author: Elias Meyer
 * Last modified and returned: 11.05.2018
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <limits.h>
#include <stdio.h>

// Constants for USART
#define BAUDRATE 9600ul
// #define OSCFREQ 16000000ul
#define UBRRVALUE (F_CPU / (16ul*BAUDRATE)-1)

// Constants which were NOT declared in the courses pdf:s
#define MAX_INT 255
#define MAX_I_TERM 100
#define SCALING_FACTOR 100
// How often should PID-controller check the voltage level
#define dt 30
#define PID_ARGUMENT_P 0.7f
#define PID_ARGUMENT_I 1.0f
#define PID_ARGUMENT_D 0.5f 

// pin which measures the voltage
#define PIN_ADC 5;

typedef struct PID_DATA {
    // Last process value, used to find derivative of process value.
    int16_t lastProcessValue;
    // Summation of errors, used for integrate calculations
    int32_t sumError;
    // The Proportional tuning constant, multiplied with SCALING_FACTOR
    int16_t P_Factor;
    // The Integral tuning constant, multiplied with SCALING_FACTOR
    int16_t I_Factor;
    // The Derivative tuning constant, multiplied with SCALING_FACTOR
    int16_t D_Factor;
    // Maximum allowed error, avoid overflow
    int16_t maxError;
    // Maximum allowed sumerror, avoid overflow
    int32_t maxSumError;
} pid_data_t;

// Atmegas current runtime. Idea is to use one timer to grow these
// so we would always know how long our microcontroller has been running.
// TODO doesn't work, interrupt is not generated.
uint32_t volatile milliseconds_ = 0;
uint32_t volatile microseconds_temp_ = 0; //If >1000, these convert to milliseconds

// Output pin parameters
/* The desired voltage level. 0 is 0v and 255 is 5v. */
uint8_t volatile voltage_level_ = 0;
/* Is the voltage level temporarily 0 */
uint8_t volatile paused_ = 0;
/* Is the measured analog feedback value used to correct the pwm output */
uint8_t pid_enabled_ = 1;
// Flag if something is to be printed
uint8_t printflag_ = 0;
uint8_t enable_printfag_ = 1;

/* DEBUG VARIABLES*/
/* How much did pid change the value last time */
int16_t change_ = 0;
/* Temporary storage for last adc read value (for print)*/
uint8_t adc_value_ = 0;

int16_t pid_p_term_ = 0;
int16_t pid_i_term_ = 0;
int16_t pid_d_term_ = 0;
int16_t pid_change_ = 0;

/* Global pid settings struct instance */
pid_data_t global_pid_settings;

//*****************************************************************
// Function declarations
//*****************************************************************

// Init functions
void init_usart0(void);
void init_timer0(void);
void init_timer1(void);
void init_adc(void);
void init_interrupt_pin2(void);
void init_pid(int16_t p_factor, int16_t i_factor, int16_t d_factor, 
              struct PID_DATA *pid);

// Serial
int serial_write(char c, FILE * filu);

// Other
void enable_pwm(uint8_t value);
void toggle_led();
uint8_t eval_char(char character);
double convert_8bit_adc_to_voltage(uint8_t value);
void set_pwm_output(uint8_t value);
int16_t pid_controller(int16_t setPoint, int16_t processValue, 
                       struct PID_DATA *pid_st);

// Arduino style setup and loop
void setup();
void loop();

// Create own outputstrea
static FILE mystdout = FDEV_SETUP_STREAM(serial_write, NULL, _FDEV_SETUP_WRITE);

//*****************************************************************
// Functions, init
//*****************************************************************

/**
 * @brief Inits USART with defined constants 
 */
void init_usart0(void)
{
    // Prescaler value
    UBRR0 = UBRRVALUE;
    
    /* Next the frame format and USART mode
    Asynchronous operation UMSEL01:0 = b00
    No parity bit UPM01:0 = b00
    One Stop bit USBS0 = 0
    8 data bits UCSZ02:0 = 0b011 
    UCPOL0 = 0 (not used in asynchronous mode)
    */
    UCSR0C |= ((1 << UCSZ01) | (1 << UCSZ00));
    
    // enable RX complete interrupt, TX complete interrupt, Data register,
    //  USART0 receiver and transmitter
    UCSR0B |= ((1 << (RXCIE0) | 0 << (TXCIE0)
            | (1 << TXEN0) | (1 << RXEN0)));
}

/**
 * @brief Inits timer0 to use fast pwm. 
 *  Channel A is used for pwm for pin 6
 *  Channel B is used to make interrupts for the internal clock system. 
 *  It doesn't work.
 */
void init_timer0(void)
{
    // The TOP-value (how long is the PWM pulse)
    OCR0A = 0; // Compare register for OC0A (pin 6)
    OCR0B = 0; // Compare register for OC0B (pin 5)
    
    // Table 19-4: For fast pwm mode, non-inverting mode
    TCCR0A |= (1 << COM0A1) | (0 << COM0A0); // OC0A
    TCCR0A |= (0 << COM0B1) | (0 << COM0B0); // OC0B

    // Table 19-9. Mode 7, Fast PWM, top=OCRA
    TCCR0A |= ((1 << WGM01) | (1 << WGM00));
    TCCR0B |= ((0 << WGM02));
	
    // Set clock to clk I/O with no prescaling (table 15-9)
    TCCR0B |= (0 << CS02) | (0 << CS01) | (1 << CS00);

    //TIMSK0 |= (1 << TOIE0);	// Timer overflow interrupt enable
	//TIMSK0 |= (1 << OCIE0B);
	
/*
    TCCR0B |= 0b10;			// prescaler 8
    TIMSK0 |= (1<<TOIE0);	// Timer overflow interrupt enable
    TCCR0A |= (1<<COM0A1);	// Fast PWM
    TCCR0A |= (1<<WGM00);	// Normaalimoodi WGM[2:0] = 0b011
    TCCR0A |= (1<<WGM01);
*/
    // Set pins as output that pwm w�rks
    DDRD |= (1 << 6);
}

/**
 * @brief Init timer 1 to generate interrupts for ADC and eventually 
 *  for the PID-controller
 */
void init_timer1(void)
{
    // The demo was made with 60000, but the one with 460 ohm 
    // resistor was made with 40000
    uint16_t top = 60000;
    /* Table 20-3, Normal port operation.
     * Table 20-6, CTC (mode 12)       
     */
    TCCR1A |= 0;
    // Waweform generation mode, clock select (20.14.2)
    TCCR1B |= ((1 << WGM13) | (1 << WGM12) | 
        (0 << CS12) | (1 << CS11) | (0 << CS10));
    TCCR1C |= 0;
    
    // Set top value for counter
    ICR1H = ((top >> 8) & 0xff);
    ICR1L = (top & 0xff);
    
    TIMSK1 |= ((1 << OCIE1B));
}

/**
 * @brief Init analog to digital converter to 8-bit mode
 *  and to start conversion from timer1
 */
void init_adc(void)
{
    // ADC enable, Auto trigger enable, ADC interrupt enable
    ADCSRA |= (1 << ADEN) | (1 << ADATE) | (1 << ADIE);
    // Clear possible waiting interrupt
    ADCSRA &= ~(1 << ADIF);
    // Set ADC prescaler division factor to 128
    ADCSRA |= 7;
    // Trigger source: timer1 compare b match
    ADCSRB |= ((1 << ADTS2) | (0 << ADTS1) | (1 << ADTS0));
    // Set correct bits to ADMUX (select input) 
    ADMUX |= PIN_ADC;
    // Set ADLAR for 8-bit conversion and reference as AVCC;
    ADMUX |= (1 << ADLAR) | (1 << REFS0);
 
    DIDR0 |= (1<<ADC0D);
}

/**
 * @brief Interrupt for the button on pin 2. Used to enable pause-function.
 */
void init_interrupt_pin2(void)
{
    DDRD &= ~(1 << DDD2);     // Clear the PD2 pin
    // PD2 (PCINT0 pin) is now an input

    PORTD |= (1 << PORTD2);    // turn On the Pull-up
    // PD2 is now an input with pull-up enabled

    // set INT0 to trigger on rising edge
    EICRA |= (1 << ISC01) | (0 << ISC00);    
    EIMSK |= (1 << INT0);     // Turns on INT0 
}

/**
 * @brief Inits PID-controller with given arguments
 * @param p_factor Factor for the proportional term (simple error term)
 * @param i_factor Factor for the integral term (error over time)
 * @param d_factor Factor for the derivative term (error speed)
 * @param pid struct PID_DATA
 */
void init_pid(int16_t p_factor, int16_t i_factor, int16_t d_factor, 
              struct PID_DATA *pid)
{
    // Start values for PID controller
    pid->sumError = 0;
    pid->lastProcessValue = 0;
    // Tuning constants for PID loop
    pid->P_Factor = p_factor;
    pid->I_Factor = i_factor;
    pid->D_Factor = d_factor;
    // Limits to avoid overflow
    pid->maxError = MAX_INT / (pid->P_Factor + 1);
    pid->maxSumError = MAX_I_TERM / (pid->I_Factor + 1);
}

//*****************************************************************************
// Functions, serial interface
//*****************************************************************************


/**
 * @brief Function for printing a character into serial
 * @param c Byte to be printed
 * @param filu
 */
int serial_write(char c, FILE * filu)
{
    while ( (UCSR0A & (1 << UDRE0)) == 0 );
    UDR0 = c; // send
    return 0;
}

//*****************************************************************************
// Functions, other
//*****************************************************************************

/** 
 * @brief Enables or disables pwm for pin 5. 
 * @param value uint8_t 0 for disable, else enable
 */
void enable_pwm(uint8_t value)
{
    if (value)
    {
        // Start PWM clock with no prescaling (CS02-CS00: 001)
        TCCR0B |= (0 << CS02) | (0 << CS01) | (1 << CS00);
    }
    else
    {
        // No clock source, stop timer (CS02-CS00: 000)
        TCCR0B &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
    }
}

/** 
 * @brief Toggles pin 13 state (The pin with led)
 */
void toggle_led()
{
    PORTB ^= (1 << 5);
}

/**
 * @brief Converts ascii characters from 0 to 5 to integers and maps 
 *  them to range [0,255]
 * @param character ascii-character to be converted
 * @return unigned char with value from 0 to 255. Unsupported characters
 *  return always 0.
 */
uint8_t eval_char(char character)
{ 
	uint8_t i = 0;
    switch (character) {
        case '0': i = 0; break;
        case '1': i = 1; break;
        case '2': i = 2; break;
        case '3': i = 3; break;
        case '4': i = 4; break;
        case '5': i = 5; break;
    }
	return 255*i/5;
}

/**
 * @brief Converts 8-bit adc value to double.
 * @return double, Voltage level 
 */
double convert_8bit_adc_to_voltage(uint8_t value)
{
    double result = (double)value/51.0;
    return result;
}

/**
 * @brief Sets the pin output pwm to correspond value. 
 * @param value uint8_t Duty cycle of pwm.
 *  0: pin always low, 255, pin always high
 */
void set_pwm_output(uint8_t value)
{
    OCR0A = value;
}

/**
 * @brief PID-controller. Coefficients are stored in struct PID_DATA.
 *  Sepoint is voltage_level_ and processValue is the ADC result.
 * @return int16_t how much the value should be changed
 */
int16_t pid_controller(int16_t setPoint, int16_t processValue, 
                       struct PID_DATA *pid_st)
{
    int16_t error, p_term, d_term;
    int32_t i_term, ret, temp;
    
    error = setPoint - processValue;
    
    // Calculate Pterm and limit error overflow
    if (error > pid_st->maxError) {
        p_term = MAX_INT;
    }
    else if (error < -pid_st->maxError) {
        p_term = -MAX_INT;
    }
    else  {
        p_term = pid_st->P_Factor * error;
    }
    
    // Calculate Iterm and limit integral runaway
    temp = pid_st->sumError + error;
    if (temp > pid_st->maxSumError) {
        i_term = MAX_I_TERM;
        pid_st->sumError = pid_st->maxSumError;
    }
    else if (temp < -pid_st->maxSumError) {
        i_term = -MAX_I_TERM;
        pid_st->sumError = -pid_st->maxSumError;
    }
    else {
        pid_st->sumError = temp;
        i_term = pid_st->I_Factor * pid_st->sumError;
    }
    
    // Calculate Dterm
    d_term = pid_st->D_Factor * (pid_st->lastProcessValue - processValue);
    
    pid_st->lastProcessValue = processValue;
    
    ret = (p_term + i_term + d_term) / SCALING_FACTOR;
    if (ret > MAX_INT) {
        ret = MAX_INT;
    }
    else if (ret < -MAX_INT){
        ret = -MAX_INT;
    }
    
    pid_p_term_ = p_term;
    pid_i_term_ = i_term;
    pid_d_term_ = d_term;
    pid_change_ = ret;
    
    return((int16_t)ret);
}

//*****************************************************************************
// Functions, interrupt routines
//*****************************************************************************

/**
 * @brief Pauses the pwm output on button press. 
 *  When pressed, output drops to 0v. Once pressed again, 
 *  ouput gets the last set voltage.
 */
ISR(INT0_vect)
{
    cli();
    /* if paused, output must read 0v. Otherwise last given value. 
      Led 13 is lit to indicate pause. 
      If currently in pause, start pwm, otherwise stop it.
      */
    if (paused_)
    {
        // Start PWM clock
        // enable_pwm(1);
        set_pwm_output(voltage_level_);
        paused_ = 0;
        PORTB &= ~(1 << 5);
    }
    else
    {
        // Stop pwm
        // enable_pwm(0);
        set_pwm_output(0);
        paused_ = 1;
        PORTB |= (1 << 5);
    }
    sei();
}

/**
 * @brief This timer should increment the milliseconds value but the interrupt is not
 * generated. That's why this doesn't work.
 */
ISR(TIMER0_COMPB_vect)
{
    cli();
	microseconds_temp_ += 16;
	if (microseconds_temp_ >= 1000)
	{
		milliseconds_ += 1;
		microseconds_temp_ -= 1000;
	}
	sei();
}

/**
 * @brief Timer 1 ready
 */
ISR(TIMER1_COMPB_vect)
{
    // Don't do anything, timer 1 is used to start ADC conversion.
}

/**
 * @brief USART RX complete 
 *  Read new information from USART and store it into
 *  usart_buffer.
 */
ISR(USART_RX_vect)
{
    cli();
	voltage_level_ = eval_char(UDR0);
    if (!paused_)
    {
        OCR0A = voltage_level_;
    }
    printflag_ = 1;
    sei();
}

/**
 * @brief ADC is ready, take the result and calculate with PID 
 * how much the output should be changed
 */
ISR(ADC_vect)
{
    cli();
    // If pid control is not enabled or pause is enabled, don't touch output.
    if (!pid_enabled_ || paused_ || voltage_level_ == 0 || voltage_level_ == 255 ) 
    {
        return;
    }
    
    adc_value_ = ADCH;
    change_ = pid_controller(voltage_level_, adc_value_, &global_pid_settings);
    OCR0A += change_;
    printflag_ = 1;
    
    sei();
}

//*****************************************************************************
// Functions, main and Arduino style setup & loop
//*****************************************************************************

/**
 * @brief This functions calls all init functions. This is called once from main.
 */ 
void setup()
{
	init_usart0();
	init_timer0();
	init_interrupt_pin2();
    init_timer1();
    init_adc();
    init_pid(PID_ARGUMENT_P, PID_ARGUMENT_I, PID_ARGUMENT_D, &global_pid_settings);
    
    // Set file output stream
    stdout = &mystdout;
    
	/* Set pin 13 as ouput and to low (led off) */
	DDRB |= (1 << PIN5);
	PORTB  &= ~(1 << PIN5);
	
	sei();
	voltage_level_ = 0;
    paused_ = 0;

	OCR0A = 0;
	OCR0B = 0;
    
    printf("Hello world?\r\n");
}

/**
 * @brief This function loops over and over again
 */
void loop()
{
    if (printflag_ && enable_printfag_)
    {
        printflag_ = 0;
        uint8_t pwm = OCR0A;
        float voltage = (float)adc_value_ / 51.0;
        printf("V_SET: %3d, OCR0A: %3d, ADC: %3d, Voltage: %01.03f, PID_P: %5d, PID_I: %5d, PID_D: %5d, PID_CHANGE: %5d\r\n", 
               voltage_level_, pwm, adc_value_, voltage, pid_p_term_, pid_i_term_, pid_d_term_, pid_change_);
        adc_value_ = 0;
    }
    
}

int main(void)
{
	setup();
    for (;;)
    {
        loop();
    }
}
