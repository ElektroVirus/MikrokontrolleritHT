#include "ringlist.h"
#include <stdlib.h>
#include <stdio.h>

void init_ringlist(struct Ringlist *list, unsigned length)
{
	if (length < 2)
		return;
    list->arr = (RINGLIST_DATATYPE *) malloc(sizeof(RINGLIST_DATATYPE) * length);
    list->MAX_SIZE = length;
    list->start = list->arr;
    list->end = 0;
    list->size = 0;
	*(list->start) = 0;
}

void free_ringlist(struct Ringlist * list)
{
    free(list->arr);
//    free(list);
}

RINGLIST_DATATYPE peek_front(struct Ringlist * list)
{
    return *(list->start);
}

RINGLIST_DATATYPE pop_front(struct Ringlist * list)
{
	if (list->size == 0)
		return 0;
    RINGLIST_DATATYPE value = peek_front(list);
    list->start++;
	list->size--;
    // If list start points now too far, set it back to start
    if ( list->start == (list->arr + list->MAX_SIZE) )
    {
        list->start = list->arr;
    }
	// If list is now empty
	if ( list->size == 0 ) {
		list->end =0;
	}
	return value;
}

/** 
 * @brief adds new element to the back of the list. 
 * @param list Ringlist 
 * @param newval int new value to be added to list
 * @return int
	0: addition successful
	1: addition succesful, list now full
	2: No commits to list, it was and is full.
 */
int push_back(struct Ringlist * list, RINGLIST_DATATYPE newval)
{
    // If list is empty
	if ( list->size == 0) {
		list->end = list->start -1;
	}
	// If list is full do nothing
	else if ( list->size == list->MAX_SIZE ){
		return 2;
	}
	
	list->end++;
	// If list end is over bounds, set it to beginning
	if ( list->end == (list->arr + list->MAX_SIZE) ){
		list->end = list->arr;
	}
	
	*(list->end) = newval;
	list->size++;
	
	// If list is now full
	if ( list->size == list->MAX_SIZE )
		return 1;
	return 0;
}

void clear(struct Ringlist * list)
{
	unsigned i = list->size;
	for ( ; i > 0; i--)
	{
		pop_front(list);
	}
}

unsigned int size(struct Ringlist * list)
{
	return list->size;
}

void tulosta(struct Ringlist * list)
{
	printf("[");
	for (unsigned int i = 0; i < list->MAX_SIZE; i++)
    {
        printf("%2d; ", *(list->arr + i));
    }
	printf("]\tStart: %ld, End: %ld", list->start - list->arr, list->end - list->arr);
	printf(", Size: %d\t{", list->size);
    
    for (unsigned int i = 0; i < list->size; i++)
    {
		RINGLIST_DATATYPE * ptr = list->start + i;
		if (ptr >= (list->arr + list->MAX_SIZE)){
			ptr -= list->MAX_SIZE;
		}
        printf("%d; ", *ptr);
    }
    printf("}\t");
    printf("\n");
}
